//
//  WeatherClientTests.swift
//  Outside
//
//  Created by Raheel Ahmad on 6/25/15.
//  Copyright © 2015 Raheel Ahmad. All rights reserved.
//

import XCTest
import Nimble
@testable import Outside

class WeatherClientTests: XCTestCase {
    
    func testWeatherURL() {
        let client = WeatherClient.defaultClient()
        let zip = "45602"
        let expectedURL = NSURL(string: "http://api.openweathermap.org/data/2.5/weather?zip=45602,us")!
        expect(client.URLForZip(zip)) == expectedURL
    }
    
    func testIsCached() {
        let session = MockURLSession()
        session.shouldMockRequest = true
        let client = WeatherClient(session: session)
        
        let zip = "49339"
        XCTAssertFalse(client.isCachedForZip(zip), "Shouldn't be cached for a zip never fetched")
        
        // make a request
        client.fetchWeatherForZip(zip) { weather in
            XCTAssertTrue(weather != nil)
        }
        
        XCTAssertTrue(client.isCachedForZip(zip), "Should be cached for a fetched zip")
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
}
