//
//  WeatherTests.swift
//  Outside
//
//  Created by Raheel Ahmad on 6/25/15.
//  Copyright © 2015 Raheel Ahmad. All rights reserved.
//

import XCTest
import Nimble
@testable import Outside

class WeatherTests: XCTestCase {
    func testWeather() {
        let weather = Weather(kelvin: 302.0, condition: 0, place: "Berkeley")
        let k = Double(weather.temperatureIn(unit: .Kelvin))
        let f = Double(weather.temperatureIn(unit: .Fahrenheit))
        let c = Double(weather.temperatureIn(unit: .Celsius))
        expect(k) ≈ 302.0
        expect(f) ≈ 83.93
        expect(c) ≈ 28.85
    }
    
    func testWeatherParsing() {
        guard let validJSON = JSONFromFile("valid_sample_response"),
              let invalidJSON = JSONFromFile("invalid_sample_response") else {
            XCTFail("Could not find sample JSON response")
            return
        }
        
        expect(Weather.fromJSON(validJSON)?.temperatureIn(unit: .Kelvin)) == 297.01
        expect(Weather.fromJSON(invalidJSON)).to(beNil())
    }
    
    func testJacketReminder() {
        let coldWeather = Weather(kelvin: 220, condition: 0, place: "Berkeley")
        let warmWeather = Weather(kelvin: 320, condition: 0, place: "Berkeley")
        expect(coldWeather.jacketReminder!) == "Wear a Jacket!"
        expect(warmWeather.jacketReminder).to(beNil())
    }
    
    func testRainReminder() {
        let rainy = Weather(kelvin: 0, condition: 500, place: "Berkeley")
        let dry = Weather(kelvin: 0, condition: 120, place: "Berkeley")
        expect(rainy.rainReminder!) == "Bring an Umbrella!"
        expect(dry.rainReminder).to(beNil())
    }
    
    func testSunnyReminder() {
        let sunny = Weather(kelvin: 0, condition: 800, place: "Berkeley")
        let notSunny = Weather(kelvin: 0, condition: 120, place: "Berkeley")
        expect(sunny.sunnyReminder!) == "Wear your sunscreen!"
        expect(notSunny.sunnyReminder).to(beNil())
    }
    
    func JSONFromFile(filename: String) -> [String: AnyObject]? {
        do {
            guard let validPath = NSBundle.mainBundle().pathForResource(filename, ofType: "json"),
                let validJSONData = NSData(contentsOfFile: validPath),
                let JSON = try NSJSONSerialization.JSONObjectWithData(validJSONData, options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject]
                else {
                    return nil
            }
            return JSON
        } catch {
            XCTFail("Could not load JSON Data")
        }
        return nil
    }

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

}
