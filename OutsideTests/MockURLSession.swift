//
//  MockURLSession.swift
//  Outside
//
//  Created by Raheel Ahmad on 6/25/15.
//  Copyright © 2015 Raheel Ahmad. All rights reserved.
//

import Foundation

class MockURLSession: NSURLSession {
    var shouldMockRequest = false
    
    override func dataTaskWithRequest(request: NSURLRequest, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void) -> NSURLSessionDataTask? {

        if shouldMockRequest {
            let path = NSBundle.mainBundle().pathForResource("valid_sample_response", ofType: "json")
            let data = NSData(contentsOfFile: path!)
            completionHandler(data, nil, nil)
            return nil
        } else {
            return super.dataTaskWithRequest(request, completionHandler: completionHandler)
        }
    }
    
    func JSONFromFile(filename: String) -> [String: AnyObject]? {
        do {
            guard let validPath = NSBundle.mainBundle().pathForResource(filename, ofType: "json"),
                let validJSONData = NSData(contentsOfFile: validPath),
                let JSON = try NSJSONSerialization.JSONObjectWithData(validJSONData, options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject]
                else {
                    return nil
            }
            return JSON
        } catch {
            return nil
        }
    }

}
