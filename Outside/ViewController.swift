//
//  ViewController.swift
//  Outside
//
//  Created by Raheel Ahmad on 6/25/15.
//  Copyright © 2015 Raheel Ahmad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var unitButton: UIButton!
    
    @IBOutlet weak var jacketReminderLabel: UILabel!
    @IBOutlet weak var rainReminderLabel: UILabel!
    @IBOutlet weak var sunnyReminderLabel: UILabel!
    
    var currentUnit: Unit = .Kelvin {
        didSet {
            unitButton.setTitle(currentUnit.rawValue, forState: .Normal)
            updateWeather()
        }
    }
    var weather: Weather? {
        didSet { updateWeather() }
    }
    
    let client = WeatherClient.defaultClient()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        temperatureLabel.hidden = true
        currentUnit = .Kelvin
        
        locationField.delegate = self
    }

    @IBAction func fetchWeather() {
        locationField.resignFirstResponder()
        activityIndicator.startAnimating()
        UIView.animateWithDuration(0.5) {
            self.temperatureLabel.hidden = true
        }
        guard let zipString = locationField.text else { return }
        client.fetchWeatherForZip(zipString) { weather in
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.weather = weather
            }
        }
    }
    
    func updateWeather() {
        let reminderSetup: (String?, UILabel) -> ()  = {
            $1.text = $0?.uppercaseString
            $1.hidden = $0 == nil
        }
        
        reminderSetup(weather?.jacketReminder, jacketReminderLabel)
        reminderSetup(weather?.rainReminder, rainReminderLabel)
        reminderSetup(weather?.sunnyReminder, sunnyReminderLabel)
        
        guard let temperature = weather?.temperatureIn(unit: currentUnit) else { return }
        let place = weather?.place
        let placeSuffix = place.flatMap { " in \($0)" } ?? ""
        
        temperatureLabel.text = "Currently it is \(Int(temperature))° \(currentUnit.rawValue)\(placeSuffix)"
        UIView.animateWithDuration(0.5) {
            self.temperatureLabel.hidden = false
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let unitsViewController = segue.destinationViewController as? UnitChoiceViewController else { return }
        
        unitsViewController.selectedUnit = currentUnit
        unitsViewController.completion = { self.currentUnit = $0 }
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        fetchWeather()
        return true
    }
}
