//
//  WeatherClient.swift
//  Outside
//
//  Created by Raheel Ahmad on 6/25/15.
//  Copyright © 2015 Raheel Ahmad. All rights reserved.
//

import Foundation

typealias FetchCompletion = (Weather?) -> ()

class WeatherClient {
    let session: NSURLSession
    let URL = NSURL(string: "http://api.openweathermap.org/data/2.5/weather")!
    var cache: [ String: (Weather, NSDate)] = [:]
    var cacheExpiry = 15.0 * 60.0
    
    class func defaultClient() -> WeatherClient {
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        return WeatherClient(session: session)
    }
    
    init(session: NSURLSession) {
        self.session = session
    }
    
    func fetchWeatherForZip(zip: String, completion: FetchCompletion) {
        if isCachedForZip(zip) {
            let cacheInfo = cache[zip]!
            let interval = NSDate().timeIntervalSinceDate(cacheInfo.1)
            if interval <= cacheExpiry {
                completion(cacheInfo.0)
                return
            }
        }
        
        guard let URL = URLForZip(zip) else {
            completion(nil)
            return
        }
        
        let request = NSURLRequest(URL: URL)
        let task = session.dataTaskWithRequest(request) { data, response, error in
            guard let data = data else {
                completion(nil); return
            }
            
            do {
                if let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject] {
                    let weather = Weather.fromJSON(json)
                    // cache
                    if let weather = weather {
                        self.cache[zip] = (weather, NSDate())
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        completion(weather)
                    }
                }
            } catch {
                dispatch_async(dispatch_get_main_queue()) {
                    completion(nil)
                }
            }
        }
        task?.resume()
    }
    
    func cachedForZip(zip: String) -> Weather? {
        return cache[zip].flatMap { $0.0 }
    }
    
    func isCachedForZip(zip: String) -> Bool {
        return cachedForZip(zip) != nil
    }
    
    func URLForZip(zip: String) -> NSURL? {
        guard let components = NSURLComponents(URL: URL, resolvingAgainstBaseURL: false) else { return nil }
        let query = NSURLQueryItem(name: "zip", value: "\(zip),us")
        components.queryItems = [ query ]
        return components.URL
    }
}
