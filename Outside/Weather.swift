//
//  Weather.swift
//  Outside
//
//  Created by Raheel Ahmad on 6/25/15.
//  Copyright © 2015 Raheel Ahmad. All rights reserved.
//

import Foundation

enum Unit: String {
    case Kelvin = "K"
    case Fahrenheit = "F"
    case Celsius = "C"
}

struct Weather {
    private let temperatureInKelvin: Float
    private let condition: Int
    let place: String
    
    static func fromJSON(json: [String: AnyObject]) -> Weather? {
        if let mainDict = json["main"] as? [String: AnyObject],
           let temperature = mainDict["temp"] as? Float,
           let weathers = json["weather"] as? [ [String:AnyObject] ],
           let firstWeather = weathers.first,
           let condition = firstWeather["id"] as? Int,
           let place = json["name"] as? String
        {
            return Weather(kelvin: temperature, condition: condition, place: place)
        } else {
            return nil
        }
    }
    
    init(kelvin: Float, condition: Int, place: String) {
        temperatureInKelvin = kelvin
        self.condition = condition
        self.place = place
    }
}

extension Weather { // MARK: Reminders
    var jacketReminder: String? {
        return temperatureInKelvin < 278 ? "Wear a Jacket!" : nil
    }
    
    var sunnyReminder: String? {
        return condition == 800 ? "Wear your sunscreen!" : nil
    }
    
    var rainReminder: String? {
        switch condition {
        case 500...531:
            return "Bring an Umbrella!"
        default:
            return nil
        }
    }
}

extension Weather { // MARK: Unit
    func temperatureIn(unit unit: Unit) -> Float {
        switch unit {
        case .Kelvin:
            return temperatureInKelvin
        case .Fahrenheit:
            return temperatureInKelvin * 1.8 - 459.67
        case .Celsius:
            return temperatureInKelvin - 273.15
        }
    }
}
