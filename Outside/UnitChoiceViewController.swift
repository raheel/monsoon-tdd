//
//  UnitChoiceViewController.swift
//  Outside
//
//  Created by Raheel Ahmad on 6/25/15.
//  Copyright © 2015 Raheel Ahmad. All rights reserved.
//

import UIKit

class UnitChoiceViewController: UITableViewController {
    let units: [Unit] = [ .Kelvin, .Fahrenheit, .Celsius ]
    
    var selectedUnit = Unit.Kelvin
    var completion: (Unit -> ())?
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return units.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("unit", forIndexPath: indexPath)
        let unit = units[indexPath.row]
        cell.textLabel?.text = unit.rawValue
        cell.accessoryType = unit == selectedUnit ? .Checkmark : .None
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedUnit = units[indexPath.row]
        completion?(selectedUnit)
        navigationController?.popViewControllerAnimated(true)
    }
}
